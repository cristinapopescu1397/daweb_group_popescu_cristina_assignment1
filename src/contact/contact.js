import React from "react";
import main from './main.css'
import contact from '../images/contactlogo.png'
import mailme from '../images/mailme.png';
import {SocialIcon} from 'react-social-icons';
import {Jumbotron} from "react-bootstrap";


class Contact extends React.Component{
    render() {
        return(
            <Jumbotron>
            <section className="container-1">

                <img id="contact" src={contact} width="180" height="180" alt="contactlogo"/>
                <h3> <strong>Cristina Popescu</strong></h3>
                <h4>Technical University of Cluj-Napoca</h4>
                <h5>Email: cristina.popescu97@yahoo.com</h5>
                <a id="mail" href="https://mail.google.com/mail/?view=cm&fs=1&to=cristina.popescu97@yahoo.com"><img id="mailmelogo" src={mailme} alt="mail me"/>Click Here To Send Mail</a>
                <h5>Phone: 0744195713</h5>

                <SocialIcon url="https://www.facebook.com/cristina.popescu.587"/>
                <SocialIcon url="https://www.linkedin.com/in/cristina-popescu-7863aa150/"/>
            </section>
            </Jumbotron>
        );
    }
}

export default Contact;