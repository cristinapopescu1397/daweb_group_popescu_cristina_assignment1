import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import News from './news/news'
import About from "./about/about";
import Profile from "./profile/profile";
import Coordinator from "./coordinator/coordinator";
import Contact from "./contact/contact";
import background from "./images/background.png"
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${background})`
};
class App extends React.Component {

    render() {
        return (
            <div style={backgroundStyle}>
            <Router>
                <div className="container nav-fill">
                    <NavigationBar/>
                    <Switch>
                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />
                        <Route
                            exact
                            path='/news'
                            render={() => <News/>}
                        />
                        <Route
                            exact
                            path='/about'
                            render={() => <About/>}
                        />
                        <Route
                            exact
                            path='/profile'
                            render={() => <Profile/>}
                        />
                        <Route
                            exact
                            path='/coordinator'
                            render={() => <Coordinator/>}
                        />
                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact/>}
                        />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
