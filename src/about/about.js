import React from "react";
import {Container, Jumbotron} from "react-bootstrap";
import film from "../images/film.mp4"
import {List, ListItem} from "react-mdl";
const divStyle = {
    display: 'flex',
    alignItems: 'left',
};
class About extends React.Component{
    render() {
        return(
            <div>
                <Jumbotron>
                    <h1>Indoor Navigation System</h1>
                    <br/>
                    <h4>
                        What are indoor navigation project's goals?
                    </h4>
                    <Container style={divStyle}>
                        <p>
                            <video src={film} width="500" height="400" controls="controls" autoPlay="false" />
                        </p>
                            <List>
                                <br/> <br/>
                                <ListItem>
                                    <p>High accuracy: The application should consistently guide users to their
                                        destinations within a reasonable distance.</p>
                                </ListItem>
                                <ListItem>
                                    <p>
                                        Intuitive user interface (UI): The application should have an easy-to-use
                                        UI that displays navigation hints correctly based on the user’s current
                                        state. The application should also take into account the obstacles surrounding the user to avoid displaying any incorrect hints. For instance,
                                        it should not tell users to go straight if there is an obstacle immediately
                                        ahead of them.
                                    </p>
                                </ListItem>
                                <ListItem>
                                    <p>
                                        No pre-loaded indoor maps: The application should be able to navigate
                                        the user without requiring a pre-loaded map of the environment.
                                    </p>
                                </ListItem>
                            </List>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default About;