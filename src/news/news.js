import React from "react";
import Card from "react-bootstrap/Card";

class News extends React.Component{

    render() {
        return(
            <div>
                <br/>
                <Card>
                    <Card.Header>18.03.2020</Card.Header>
                    <Card.Body>
                        <Card.Title>Finished Assignment</Card.Title>
                        <Card.Text>
                            done
                        </Card.Text>
                    </Card.Body>
                </Card>
                <br/>
                <Card>
                    <Card.Header>15.03.2020</Card.Header>
                    <Card.Body>
                        <Card.Title>Started Assignment</Card.Title>
                        <Card.Text>
                            in progress
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

export default News;