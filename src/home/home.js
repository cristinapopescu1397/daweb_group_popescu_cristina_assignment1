import React from 'react';
import {Jumbotron} from 'reactstrap';
import carousel1 from '../images/carusel1.png';
import carousel2 from '../images/carusel2.png'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import Container from "react-bootstrap/Container";
const car = {
    height: "480px"
};
const cul = {
    color: "#d2c7d4"
};
class Home extends React.Component {
    render() {
        return (
            <div>
                <Jumbotron>
                    <h3>Indoor Navigation System</h3>

                    <p style={cul}> <b>Indoor navigation deals with navigation within buildings. Because GPS reception is normally non-existent inside buildings, other positioning technologies are used here when automatic positioning is desired. Wi-Fi or beacons (Bluetooth Low Energy, BLE) are often used in this case to create a so-called "indoor GPS".</b></p>
                    <Carousel dynamicHeight="true">
                        <div style={car}>
                            <img align="middle" src={carousel1}  alt="sd"/>
                        </div>
                        <div style={car}>
                            <img src={carousel2} alt="dd"/>
                        </div>
                    </Carousel>
                </Jumbotron>
            </div>
        )
    };
}

export default Home
