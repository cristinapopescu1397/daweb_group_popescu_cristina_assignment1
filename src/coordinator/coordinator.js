import React from "react";
import {List, ListItem, ListItemContent} from 'react-mdl'
import {Container, Jumbotron} from "react-bootstrap";
import madalin from '../images/madalin.png'
const divStyle = {
    display: 'flex',
    alignItems: 'center'
};
class Coordinator extends React.Component{
    render() {
        return(
            <div>
                <Jumbotron fluid style={divStyle}>
                    <Container>
                        <img
                            className="avatar-img"
                            src={madalin}
                            alt="avatar"
                        />
                        <h2 align="center">Madalin Neagu</h2>
                    </Container>
                    <br/>
                    <Container>
                        <h3>Teaching</h3>
                        <hr/>

                        <div className="contact-list">
                            <List>
                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        Programming in Assembly Language
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        ​Microprocessor-based Systems
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        Structure of Computer Systems
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        Industrial Informatics
                                    </ListItemContent>
                                </ListItem>


                            </List>
                        </div>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default Coordinator;