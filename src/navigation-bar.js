import React from 'react'
import './navigation-bar.css'
import {
    Nav,
    Navbar,
    NavLink
} from 'reactstrap';
import logo from './images/FAVPNG_location-icon-misc-icon-pin-icon_FQ6bbF2n.png'
import NavbarBrand from "react-bootstrap/NavbarBrand";
class NavigationBar extends React.Component{
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                <Navbar className="menu" expand="md">
                    <NavbarBrand href="/">
                        <img src={logo} width={"40"} alt="logo"/>
                    </NavbarBrand>
                    <Nav navbar>
                        <NavLink className="menu__link" href="/">
                                Home
                        </NavLink>
                        <NavLink className="menu__link" href="/news">
                                News
                        </NavLink>
                        <NavLink className="menu__link" href="/about">
                                About
                        </NavLink>
                        <NavLink className="menu__link" href="/profile">
                                Profile
                        </NavLink>
                        <NavLink className="menu__link" href="/coordinator">
                                Coordinator
                        </NavLink>
                        <NavLink className="menu__link" href="/contact">
                                Contact
                        </NavLink>
                    </Nav>
                </Navbar>
            </div>
        );
    }
};

export default NavigationBar

// class NavigationBar extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.toggle = this.toggle.bind(this);
//         this.state = {
//             isOpen: false,
//         };
//     }
//
//     toggle() {
//         this.setState({
//             isOpen: !this.state.isOpen
//         });
//     }
//
//     onClick() {
//         this.setState({
//             bgColor:'blue'
//         })
//     }
//
//     render() {
//         let linksMarkup = this.props.links.map((link, index) => {
//             let linkMarkup = link.active ? (
//                 <a className="menu__link menu__link--active" href={link.link}>{link.label}</a>
//             ) : (
//                 <a className="menu__link" href={link.link}>{link.label}</a>
//             );
//
//             return (
//                 <li key={index} className="menu__list-item">
//                     {linkMarkup}
//                 </li>
//             );
//         });
//
//         return (
//             <nav className="menu">
//                 <div className="menu__right">
//                     <ul className="menu__list">
//                         {linksMarkup}
//                     </ul>
//                 </div>
//             </nav>
//         );
//     }
// }
//
// export default NavigationBar;